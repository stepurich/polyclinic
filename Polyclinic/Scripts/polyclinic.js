﻿var _selectSpecialityTitle = "Выберете специализацию";
var _selectDoctorTitle = "Выберете доктора";
var _selectSpecTitle = "Выберете специалиста";
var _selectServiceTitle = "Выберете услугу";

$(function () {
    $('#free-time').bootstrapTable();
    $('#doc-records').bootstrapTable();
    $('#spec-records').bootstrapTable();
    $('#tickets').bootstrapTable();
    $('#tickets-service').bootstrapTable();

    $('#record-to-doctor-wizard').bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                if ($("#ticket-number").val() == "") {
                    alert("Введите номер карты!");
                    return false;
                }

                if ($("#ticket-number").val() != "") {
                    getData("/Polyclinic/GetDataHandler", "checkticket", $("#ticket-number").val());
                    return false;
                }
            }

            if (index == 2) {
                if ($("#post-selector .selector-title").html() == _selectSpecialityTitle) {
                    alert(_selectSpecialityTitle + "!");
                    return false;
                }
            }

            if (index == 3) {
                if ($("#doctor-selector .selector-title").html() == _selectDoctorTitle) {
                    alert(_selectDoctorTitle + "!");
                    return false;
                }
            }

            if (index == 4)
            {
                var scheduleid = $('#free-time').bootstrapTable('getSelections')[0].scheduleid;
                var time = $('#free-time').bootstrapTable('getSelections')[0].time;
                var doctor = $('#free-time').bootstrapTable('getSelections')[0].name;
                var ticketNumber = $("#ticket-number").val();

                getData("/Polyclinic/GetDataHandler",
                    "recordtodoctor",
                    ticketNumber,
                    scheduleid);


                $("#tab5").html("<p><b>Номером карты</b>: " + ticketNumber +
                            "</p><p><b>Врач</b>: " + doctor +
                            "</p><p><b>Время</b>: " + time + "</p>");
            }

            return true;
        },
        onTabShow: function (tab, navigation, index) {

            if (index == 1) {
                getData("/Polyclinic/GetDataHandler", "posts");
            }

            if (index == 2) {
                getData("/Polyclinic/GetDataHandler", "doctors", $("#post-selector").attr("post-id"));
            }

            if (index == 3) {
                getData("/Polyclinic/GetDataHandler", "doctorschedule", $("#doctor-selector").attr("doc-id"));
            }
        }
    });

    $('#viewer-records-to-doctor-wizard').bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                if ($("#doctor-selector .selector-title").html() == _selectDoctorTitle) {
                    alert(_selectDoctorTitle + "!");
                    return false;
                }
            }

            return true;
        },
        onTabShow: function (tab, navigation, index) {
            if (index == 0) {
                getData("/Polyclinic/GetDataHandler", "doctors");
            }

            if (index == 1) {
                getData("/Polyclinic/GetDataHandler", "viewdoctorsrecords", $("#doctor-selector").attr("doc-id"));
            }
        }
    });

    $('#viewer-tickets-wizard').bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                if ($("#doctor-id").val() == "") {
                    alert("Введите Ваш индивидуальный номер!");
                    return false;
                }

                if ($("#doctor-id").val() != "") {
                    getData("/Polyclinic/GetDataHandler", "checkticket", $("#card-id").val());
                    return false;
                }
            }

            return true;
        },
        onTabShow: function (tab, navigation, index) {

            if (index == 1) {
                getData("/Polyclinic/GetDataHandler", "viewdoctickets", $("#card-id").val());
                getData("/Polyclinic/GetDataHandler", "viewservicetickets", $("#card-id").val());
                
            }
        }
    });

    $('#record-on-service-wizard').bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                if ($("#ticket-number").val() == "") {
                    alert("Введите номер карты!");
                    return false;
                }

                if ($("#ticket-number").val() != "") {
                    getData("/Polyclinic/GetDataHandler", "checkticket", $("#ticket-number").val());
                    return false;
                }
            }

            if (index == 2) {
                if ($("#service-selector .selector-title").html() == _selectServiceTitle) {
                    alert(_selectServiceTitle + "!");
                    return false;
                }
            }

            if (index == 3) {

                var serviceId = $("#service-selector").attr("service-id");
                var service = $("#service-selector .selector-title").html();
                var ticketNumber = $("#ticket-number").val();
                var date = $('#date-selector').data("DateTimePicker").date().format();
                date = moment(date).format("YYYY/MM/DD");

               getData("/Polyclinic/GetDataHandler",
                       "addservrecord",
                       ticketNumber,
                       serviceId,
                       date);

                $("#tab4").html("<p><b>Номером карты</b>: " + ticketNumber +
                    "</p><p><b>Услуга</b>: " + service +
                    "</p><p><b>Специалист</b>: <span id='specId'></span></p>" +
                   "</p><p><b>Дата</b>: " + date + "</p>");

                getData("/Polyclinic/GetDataHandler", "spec");
            }

            return true;
        },
        onTabShow: function (tab, navigation, index) {

            if (index == 1) {
                getData("/Polyclinic/GetDataHandler", "services");
            }

            if (index == 2) {
                var serviceId = $("#service-selector").attr("service-id");

                getData("/Polyclinic/GetDataHandler", "busyservdates", serviceId);
            }

        }
    });

    $('#viewer-records-to-spec-wizard').bootstrapWizard({
        tabClass: 'nav nav-pills',
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                if ($("#doctor-selector .selector-title").html() == _selectSpecTitle) {
                    alert(_selectSpecTitle + "!");
                    return false;
                }
            }

            return true;
        },
        onTabShow: function (tab, navigation, index) {
            if (index == 0) {
                getData("/Polyclinic/GetDataHandler", "doctors", "speconly");
            }

            if (index == 1) {
                getData("/Polyclinic/GetDataHandler", "viewspecrecords", $("#doctor-selector").attr("doc-id"));
            }
        }
    });

});

function getData(path, args, data1, data2, data3) {
    var dataObj = { command: args, data1: data1, data2: data2, data3: data3 };
    $.ajax({
        url: path,
        data: dataObj,
        dataType: "json",
        type: "POST"
    }).done(function (responce) {
        applyRespoce(responce, args);
    });
}

function applyRespoce(responce, command) {
    if (responce.exception != null) {
        $("#body-container").hide();
        $("#exception-container").show().html(responce.exception);
        return;
    }

    $("#body-container").show();
    $("#exception-container").hide().html();

    switch (command) {
        case "posts":
            $("#posts-menu").html("");
            $("#post-selector .selector-title").html(_selectSpecialityTitle);
            $("#post-selector").attr("post-id", "");

            for (var i = 0; responce.length > i; i++) {
                var li = "<li><a href=\"javascript: postSelected('" + responce[i].id + "', '" + responce[i].name + "')\">" + responce[i].name + "</a></li>";
                $("#posts-menu").append(li)
            }
            break;
        case "doctors":
            $("#doctors-menu").html("");
            var titleCont = $("#doctor-selector .selector-title");
            if (titleCont.hasClass("spec"))
                titleCont.html(_selectSpecTitle);
            else
                titleCont.html(_selectDoctorTitle);

            $("#doctor-selector").attr("doc-id", "");

            for (var i = 0; responce.length > i; i++) {
                var li = "<li><a href=\"javascript: doctorSelected('" + responce[i].id + "', '" + responce[i].name + "')\">" + responce[i].name + "</a></li>";
                $("#doctors-menu").append(li)
            }
            break;
        case "checkticket":
            if (responce) {
                if ($('#record-to-doctor-wizard').length)
                    $('#record-to-doctor-wizard').bootstrapWizard('show', 1);

                if ($('#record-on-service-wizard').length)
                    $('#record-on-service-wizard').bootstrapWizard('show', 1);

                if ($('#viewer-tickets-wizard').length)
                    $('#viewer-tickets-wizard').bootstrapWizard('show', 1);
            }
            else
                alert("Этот номер карты не зарегистрирован!");
            break;
        case "doctorschedule":
            $('#free-time').bootstrapTable('removeAll');
            $('#free-time').bootstrapTable('destroy');
            var tr = "";
            $("#free-time tbody.free-time-body").html("");
            for (var i = 0; responce.length > i; i++) {
                tr = "<tr>";
                tr += "<td><input type='radio' name='radioGroup'></td>";
                tr += "<td>" + responce[i].cabinet + "</td>";
                tr += "<td>" + responce[i].time + "</td>";
                tr += "<td>" + responce[i].name + "</td>";
                tr += "<td>" + responce[i].scheduleid + "</td>";
                tr += "</tr>";

                $("#free-time tbody.free-time-body").append(tr)
            }

            $('#free-time').bootstrapTable();
            if (responce.length > 0)
                $('#free-time').bootstrapTable('check', 0);

            break;
        case "checkdocid":
            if (responce) {
                if ($('#viewer-records-to-doctor-wizard').length)
                    $('#viewer-records-to-doctor-wizard').bootstrapWizard('show', 1);
            }
            else
                alert("Этот индивидуальный номер врача не существует!");
            break;
        case "checkspecid":
            if (responce) {
                if ($('#viewer-records-to-spec-wizard').length)
                    $('#viewer-records-to-spec-wizard').bootstrapWizard('show', 1);
            }
            else
                alert("Этот индивидуальный номер не существует!");
            break;

        case "viewdoctorsrecords":
            var tr = "";
            $('#doc-records').bootstrapTable('removeAll');
            $('#doc-records').bootstrapTable('destroy');
            $("#doc-records tbody.doc-records-body").html("");
            for (var i = 0; responce.length > i; i++) {
                tr = "<tr>";
                tr += "<td>" + responce[i].cabinet + "</td>";
                tr += "<td>" + responce[i].time + "</td>";
                tr += "<td>" + responce[i].name + "</td>";
                tr += "</tr>";

                $("#doc-records tbody.doc-records-body").append(tr)
            }
            $('#doc-records').bootstrapTable();
            break;
        case "viewspecrecords":
            var tr = "";
            $('#spec-records').bootstrapTable('removeAll');
            $('#spec-records').bootstrapTable('destroy');
            $("#spec-records tbody.spec-records-body").html("");
           for (var i = 0; responce.length > i; i++) {
                tr = "<tr>";
                tr += "<td>" + responce[i].service + "</td>";
                tr += "<td>" + responce[i].cabinet + "</td>";
                tr += "<td>" + responce[i].time + "</td>";
                tr += "<td>" + responce[i].name + "</td>";
                tr += "<td>" + responce[i].price + "</td>";
                tr += "</tr>";

                $("#spec-records tbody.spec-records-body").append(tr)
            }
           $('#spec-records').bootstrapTable();
            break;
        case "viewdoctickets":
            var tr = "";
            $('#tickets').bootstrapTable('removeAll');
            $('#tickets').bootstrapTable('destroy');
            $("#tickets tbody.tickets-body").html("");

            for (var i = 0; responce.length > i; i++) {
                tr = "<tr>";
                tr += "<td>" + responce[i].name + "</td>";
                tr += "<td>" + responce[i].cabinet + "</td>";
                tr += "<td>" + responce[i].time + "</td>";
                tr += "</tr>";

                $("#tickets tbody.tickets-body").append(tr)
            }
            $('#tickets').bootstrapTable();
            break;
        case "viewservicetickets":
            var tr = "";
            $('#tickets-service').bootstrapTable('removeAll');
            $('#tickets-service').bootstrapTable('destroy');
            $("#tickets-service tbody.tickets-service-body").html("");
           for (var i = 0; responce.length > i; i++) {
                tr = "<tr>";
                tr += "<td>" + responce[i].service + "</td>";
                tr += "<td>" + responce[i].name + "</td>";
                tr += "<td>" + responce[i].cabinet + "</td>";
                tr += "<td>" + responce[i].time + "</td>";
                tr += "<td>" + responce[i].price + "</td>";
                tr += "</tr>";

                $("#tickets-service tbody.tickets-service-body").append(tr)
            }
           $('#tickets-service').bootstrapTable();
            break;
        case "services":
            $("#service-menu").html("");
            $("#service-selector .selector-title").html(_selectServiceTitle);
            $("#service-selector").attr("service-id", "");

            for (var i = 0; responce.length > i; i++) {
                var li = "<li><a href=\"javascript: servicesSelected('" + responce[i].id + "', '" + responce[i].name + "')\">" + responce[i].name + "</a></li>";
                $("#service-menu").append(li)
            }
            break;
        case "spec":
            $("#specId").html(responce.name);
            break;
        case "busyservdates":
            if ($('#date-selector').data("DateTimePicker")) {
                $('#date-selector').data("DateTimePicker").clear();
                $('#date-selector').data("DateTimePicker").destroy();
            }

            $('#date-selector').datetimepicker({
                format: 'YYYY/MM/DD',
                minDate: moment(),
                maxDate: moment().add(14, 'days'),
                disabledDates: responce
            });

            break;
   }
}

function postSelected(postId, postName) {
    $("#post-selector .selector-title").html(postName);
    $("#post-selector").attr("post-id", postId);
}


function doctorSelected(doctorId, doctorName) {
    $("#doctor-selector .selector-title").html(doctorName);
    $("#doctor-selector").attr("doc-id", doctorId);
}

function servicesSelected(serviceId, serviceName) {
    $("#service-selector .selector-title").html(serviceName);
    $("#service-selector").attr("service-id", serviceId);
}