//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Polyclinic.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Services
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Services()
        {
            this.Rec_serv = new HashSet<Rec_serv>();
        }
    
        public int Serv_ID { get; set; }
        public Nullable<int> Cad_ID { get; set; }
        public int Price { get; set; }
        public string Discr { get; set; }
        public Nullable<int> Pers_ID { get; set; }
    
        public virtual Cabinet Cabinet { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rec_serv> Rec_serv { get; set; }
        public virtual Spec Spec { get; set; }
    }
}
