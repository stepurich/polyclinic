﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Polyclinic;

namespace Polyclinic.Controllers
{
    public class Record_doctorController : Controller
    {
        private bd_polyclinicEntities db = new bd_polyclinicEntities();

        // GET: Record_doctor
        public ActionResult Index()
        {
            var record_doctor = db.Record_doctor.Include(r => r.Reg_card).Include(r => r.Schedule);
            return View(record_doctor.ToList());
        }

        // GET: Record_doctor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Record_doctor record_doctor = db.Record_doctor.Find(id);
            if (record_doctor == null)
            {
                return HttpNotFound();
            }
            return View(record_doctor);
        }

        // GET: Record_doctor/Create
        public ActionResult Create()
        {
            ViewBag.Rec_card_ID = new SelectList(db.Reg_card, "Rec_card_ID", "FIO");
            ViewBag.Schedule_ID = new SelectList(db.Schedule, "Schedule_ID", "Schedule_ID");
            return View();
        }

        // POST: Record_doctor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Rec_to_doc_ID,Rec_card_ID,Schedule_ID,Date")] Record_doctor record_doctor)
        {
            if (ModelState.IsValid)
            {
                db.Record_doctor.Add(record_doctor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Rec_card_ID = new SelectList(db.Reg_card, "Rec_card_ID", "FIO", record_doctor.Rec_card_ID);
            ViewBag.Schedule_ID = new SelectList(db.Schedule, "Schedule_ID", "Schedule_ID", record_doctor.Schedule_ID);
            return View(record_doctor);
        }

        // GET: Record_doctor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Record_doctor record_doctor = db.Record_doctor.Find(id);
            if (record_doctor == null)
            {
                return HttpNotFound();
            }
            ViewBag.Rec_card_ID = new SelectList(db.Reg_card, "Rec_card_ID", "FIO", record_doctor.Rec_card_ID);
            ViewBag.Schedule_ID = new SelectList(db.Schedule, "Schedule_ID", "Schedule_ID", record_doctor.Schedule_ID);
            return View(record_doctor);
        }

        // POST: Record_doctor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Rec_to_doc_ID,Rec_card_ID,Schedule_ID,Date")] Record_doctor record_doctor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(record_doctor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Rec_card_ID = new SelectList(db.Reg_card, "Rec_card_ID", "FIO", record_doctor.Rec_card_ID);
            ViewBag.Schedule_ID = new SelectList(db.Schedule, "Schedule_ID", "Schedule_ID", record_doctor.Schedule_ID);
            return View(record_doctor);
        }

        // GET: Record_doctor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Record_doctor record_doctor = db.Record_doctor.Find(id);
            if (record_doctor == null)
            {
                return HttpNotFound();
            }
            return View(record_doctor);
        }

        // POST: Record_doctor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Record_doctor record_doctor = db.Record_doctor.Find(id);
            db.Record_doctor.Remove(record_doctor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
