﻿using Polyclinic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Polyclinic.Controllers
{
    public class PolyclinicController : Controller
    {
        private bd_polyclinicEntities db = new bd_polyclinicEntities();

        //[SecretCode]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RecordToDoctor()
        {
            return View();
        }

        public ActionResult ViewRecordsToDoctor()
        {
            return View();
        }

        public ActionResult ViewRecordsOnServices()
        {
            return View();
        }

        public ActionResult ViewTickets()
        {
            return View();
        }

        public ActionResult InDeveloping()
        {
            return View();
        }

        public ActionResult RecordOnService()
        {
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetDataHandler(string command, string data1, string data2, string data3)
        {
            object responce = null;
            string cardNum = "";
            try
            {
                switch (command)
                {
                    case "posts":
                        List<object> posts = new List<object>();
                        var post = db.Specialty;
                        foreach (var p in post.ToList())
                        {
                            posts.Add(new { name = p.Discr, id = p.Spec_ID });
                        }
                        responce = posts.ToArray();
                        break;
                    case "doctors":
                        List<object> docs = new List<object>();
                        if (data1 == "speconly")
                        {
                            foreach (var staff in db.Spec.Select(x => x.Staff).ToList())
                            {
                                docs.Add(new
                                {
                                    name = staff.FIO,
                                    id = staff.Pers_ID
                                });
                            }
                            responce = docs.ToArray();
                            break;
                        }

                        int selPostId = data1 == null ? -1 : Int32.Parse(data1);
                        if (selPostId >= 0)
                        {
                            var selctedPost = db.Specialty.Include(s=>s.Staff1).FirstOrDefault(p=>p.Spec_ID == selPostId);
                            foreach (var staff in selctedPost.Staff1.ToList())
                            {
                                docs.Add(new {
                                    name = staff.FIO,
                                    id = staff.Pers_ID
                                });
                            }
                        }
                        else
                        {
                            foreach (var staff in db.Doctor.Select(x=>x.Staff).ToList())
                            {
                                docs.Add(new
                                {
                                    name = staff.FIO,
                                    id = staff.Pers_ID
                                });
                            }
                        }
                        responce = docs.ToArray();
                        break;
                    case "checkticket":
                        cardNum = data1;
                        responce = db.Reg_card.Any(x => x.Card_Num == cardNum);
                        break;
                    case "checkdocid":
                        var doc_ID = Int32.Parse(data1);
                        responce = db.Doctor.Any(x => x.Pers_ID == doc_ID);
                        break;
                    case "checkspecid":
                        var spec_ID = Int32.Parse(data1);
                        responce = db.Spec.Any(x => x.Pers_ID == spec_ID);
                        break;
                    case "doctorschedule":
                        List<object> docsRecs = new List<object>();
                        int docId = Int32.Parse(data1);
                        var doctoRrecords = db.Schedule.Include(x => x.Cabinet).
                            Where(s => s.Pers_ID == docId && s.Rec_card_ID == null);
                        foreach (var dRec in doctoRrecords)
                        {
                            docsRecs.Add(new
                            {
                                name = dRec.Doctor.Staff.FIO,
                                cabinet = dRec.Cabinet.Cab_Num,
                                time = dRec.Start_Date.ToString("yy-MM-dd H:mm:ss"),
                                scheduleid = dRec.Schedule_ID
                            });
                        }
                        responce = docsRecs.ToArray();
                        break;
                    case "recordtodoctor":
                        var sched = db.Schedule.Find(Int32.Parse(data2));
                        sched.Rec_card_ID = db.Reg_card.FirstOrDefault(x => x.Card_Num == data1).Rec_card_ID;
                        db.SaveChanges();
                        break;
                    case "viewdoctorsrecords":
                        List<object> docRecs = new List<object>();
                        int doc_Id = Int32.Parse(data1);
                        var docRrecords = db.Schedule
                            .Include(r => r.Cabinet)
                            .Include(r => r.Reg_card)
                            .Where(r => r.Pers_ID == doc_Id && r.Rec_card_ID != null);
                        foreach (var dRec in docRrecords)
                        {
                            docRecs.Add(new
                            {
                                name = dRec.Reg_card.FIO,
                                cabinet = dRec.Cabinet.Cab_Num,
                                time = dRec.Start_Date.ToString("yy-MM-dd H:mm:ss")
                            });
                        }
                        responce = docRecs.ToArray();
                        break;
                    case "viewspecrecords":
                        List<object> specRecs = new List<object>();
                        int spec_Id = Int32.Parse(data1);
                        var specRrecords = db.Rec_serv
                           .Include(r => r.Services)
                            .Include(r => r.Reg_card)
                            .Where(r => r.Services.Pers_ID == spec_Id);
                        foreach (var sRec in specRrecords)
                        {
                            specRecs.Add(new
                            {
                                name = sRec.Reg_card.FIO,
                                cabinet = sRec.Services.Cabinet.Cab_Num,
                                time = sRec.Date.Value.ToString("yyyy-MM-dd"),
                                service = sRec.Services.Discr,
                                price = sRec.Services.Price
                            });
                        }
                        responce = specRecs.ToArray();
                        break;
                    case "viewdoctickets":
                        List<object> tickRecs = new List<object>();
                        cardNum = data1;
                        var tickRrecords = db.Schedule.
                            Where(r => r.Reg_card.Card_Num == cardNum);
                        foreach (var dRec in tickRrecords)
                        {
                            tickRecs.Add(new
                            {
                                name = dRec.Doctor.Staff.FIO,
                                cabinet = dRec.Cabinet.Cab_Num,
                                time = dRec.Start_Date.ToString("yy-MM-dd H:mm:ss"),
                            });
                        }
                        responce = tickRecs.ToArray();
                        break;
                    case "viewservicetickets":
                        List<object> serTickRecs = new List<object>();
                        cardNum = data1;
                        var servTickRrecords = db.Rec_serv.Include(r => r.Services).Include(r=>r.Reg_card).
                            Where(r => r.Reg_card.Card_Num == cardNum);
                        foreach (var sRec in servTickRrecords)
                        {
                            serTickRecs.Add(new
                            {
                                service = sRec.Services.Discr,
                                name = sRec.Services.Spec.Staff.FIO,
                                cabinet = sRec.Services.Cabinet.Cab_Num,
                                time = sRec.Date.Value.ToString("yy-MM-dd H:mm:ss"),
                                price = sRec.Services.Price
                            });
                        }
                        responce = serTickRecs.ToArray();
                        break;
                    case "services":
                        List<object> servs = new List<object>();
                        var services = db.Services;
                        foreach (var serv in services.ToList())
                        {
                            servs.Add(new { name = serv.Discr, id = serv.Serv_ID });
                        }
                        responce = servs.ToArray();
                        break;
                    case "spec":
                        List<object> specs = new List<object>();
                        var servics = db.Services.Include(s => s.Spec);
                        var ser = servics.FirstOrDefault();
                        responce = new { name = ser.Spec.Staff.FIO, id = ser.Pers_ID };
                        break;
                    case "addservrecord":
                        Rec_serv recServ = new Rec_serv();
                        recServ.Rec_card_ID = db.Reg_card.FirstOrDefault(x=> x.Card_Num == data1).Rec_card_ID;
                        recServ.Serv_ID = Int32.Parse(data2);
                        recServ.Date = DateTime.Parse(data3);
                        db.Rec_serv.Add(recServ);
                        db.SaveChanges();
                        break;
                    case "busyservdates":
                        List<string> busyDates = new List<string>();
                        var rec_servs_groups = db.Rec_serv.Include(r => r.Services).ToList()
                            .Where(r => r.Serv_ID == Int32.Parse(data1))
                            .GroupBy(r => r.Date.Value.ToString("yyyy-MM-dd"));

                        //Максимальное количесво записей на услугу в день
                        var maxRecOnDate = 2;
                        foreach (var r_serv_group in rec_servs_groups)
                        {
                            if (r_serv_group.ToList().Count > maxRecOnDate - 1)
                            {
                                busyDates.Add(r_serv_group.Key);
                            }
                        }
                        responce = busyDates.ToArray();
                        break;
                }
            }
            catch (Exception e)
            {
                responce = new { exception = e.Message };
            }

            return Json(responce, JsonRequestBehavior.DenyGet);
        }
    }

    public class SecretCodeAttribute : ActionFilterAttribute
    {
        private const string SECRET_CODE = "u9l1fz63";
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var code = filterContext.HttpContext.Request.QueryString["code"]?.ToString();
            if (code == null || code.ToLower() != SECRET_CODE)
                filterContext.Result = new HttpStatusCodeResult(403);

            base.OnActionExecuting(filterContext);
        }
    }
}

